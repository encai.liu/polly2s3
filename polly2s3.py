#%% 必要なライブラリをimport
import subprocess

from boto3 import Session
from boto3  import resource
from contextlib import closing

session = Session(region_name="ap-northeast-1")
polly = session.client("polly")

s3 = resource('s3')
bucket = s3.Bucket("myttsresult")

#%% 合成してみる
# # 日本語サンプル
# voiceId = "Takumi" # 日本語男性
# # voiceId = "Mizuki" # 日本語女性
# text = "吾輩は猫である、名前がまだない。"

# # 英語サンプル
# voiceId = "Joanna"
# # voiceId = "Matthew"
# text = "I am a cat, and don't have a name yet."

# 中国語サンプル
voiceId = "Zhiyu"
text = "我是一只猫，还没有名字。"

response = polly.synthesize_speech(
    # Engine="neural",
    OutputFormat="mp3",
    VoiceId=voiceId,
    Text=text
    )

with closing(response["AudioStream"]) as stream:
    output = "polly_sample.mp3"

    # # 直接S3に保存する
    # bucket.put_object(Key=output, Body=stream.read())

    # ファイルに保存してからS3に送信
    with open(output, "wb") as file:
        file.write(stream.read())
        # bucket.upload_file(output, output) #(localpath, remotefile)

    # soxでオーディオを再生
    cmd = "play " + output
    returncode = subprocess.call(cmd)
